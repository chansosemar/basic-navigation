import React from 'react';
import {SafeAreaView} from 'react-native';

import AppStack from 'navigation';

const App: () => Node = () => {
  return (
    <>
      <AppStack />
    </>
  );
};

export default App;
